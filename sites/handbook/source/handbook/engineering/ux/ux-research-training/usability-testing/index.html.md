---
layout: handbook-page-toc
title: "Usability testing"
description: "Conducting usability testing at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


Usability testing is the process of evaluating a product experience with representative users. The aim is to observe how users complete a set of tasks and to understand any problems they encounter. Since users often perform tasks differently than expected, this qualitative method helps to uncover why users perform tasks the way that they do, including understanding their motivations and needs. At GitLab, usability testing is part of [solution validation](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/solution-validation-and-methods/).


## Different types of usability testing

Generally speaking, we can differentiate between:

**Moderated versus unmoderated usability testing**

Moderated tests have a moderator present who guides participants through the tasks. This allows them to have a conversation about their experience, and it helps to find answers to “Why?” questions. 

Conversely, users complete [unmoderated usability](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/unmoderated-testing/) tests on their own without the presence of a moderator. This is helpful when you have a very direct question.

| Moderated usability testing                                                                                                                                                                                         | Unmoderated usability testing                                                                                                                                                       |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Complex questions/WHY? Questions, such as:<br><br>  “Why do users experience a problem when using a feature?”<br>  “Why are users not successful in using a feature?”<br>  “How do users go about using a feature?” | Direct questions, such as:<br><br>  “Do users find the entry point to complete their task?"<br> “Do users recognize the new UI element?”<br> “Which design option do users prefer?” |

**Formative versus summative usability testing**

Formative usability tests are continuous evaluations to discover usability problems. They tend to have a smaller scope, such as focusing on one specific feature of a product or just parts of it. Oftentimes, prototypes are used for evaluation. 

Summative usability tests tend to be larger in scope and are run with the live product. They are useful if you lack details on why users are experiencing a particular problem or want to validate in the first place how easy it is to use. 

## Steps for conducting a usability test
1. [Establish your research question](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/defining-goals-objectives-and-hypotheses/#step-1---start-thinking-of-a-problem).
2. Identify the tasks you want to focus on for your usability test. 
    - There is no magic number when it comes to how many tasks to include. A guideline is to have [3 - 4 tasks](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/#tasks), as this ensures that participants don’t get tired and exhausted. Another aspect to consider is that unmoderated test sessions should be 15 - 20 minutes max and moderated sessions 60min max. 
    - The key thing to remember when writing your tasks is that [they reflect realistic user goals](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/#tasks). Taking the JTBD into account when creating your tasks helps keep the focus on user goals. See these [tips for writing good tasks](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/#tasks) that include examples. 
3. Define what success looks like.
    - Before testing with users, reach agreement among your stakeholders about what success looks like, such as the target completion rate you are aiming for. For example, a completion rate greater than 80% translates to a Complete or Lovable [CM scorecard level](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#calculating-the-cm-scorecard-score). 
4. [Identify your target audience and initiate recruiting](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/).
5. [Prepare your prototype or demo environment](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#step-2-prepare-your-testing-environment).
6. [Write your test script, including tasks and metrics to collect](https://docs.google.com/document/d/1_5Qu2JR9QE5LE6cK4eq9yJs-nXv2rlWWifcjacaiWdI/edit).
    - [The International Organization of Standardization’s (ISO) definition of usability](https://www.iso.org/obp/ui/#iso:std:iso:9241:-11:ed-2:v1:en) focuses on 3 factors: Effectiveness, Efficiency, Satisfaction. When setting up your usability test, we recommend capturing the metrics shown in the table below, as this will help to measure improvements consistently over time and [to assess their impact on system usability](https://about.gitlab.com/handbook/engineering/ux/#system-usability). There are many other metrics that you can measure to understand usability problems, such as error rates or number of times a user needed help. If you find them helpful for your research topic, feel free to use them.
    - Remind participants to think aloud as they go through the tasks. 
    - Take a look at these more [detailed tips and tricks](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/) on how to write an excellent usability test script. 

    
| Factor        | What’s being measured? | How are we measuring it? | Why are we measuring it?  |
|---------------|------------------------|--------------------------|---------------------------|
| Effectiveness | **Task success/completion rates per task** <br> <br> + capture WHY participants are failing  |   | We want users to succeed in reaching their goals. Understanding why they fail will help to improve the experience.|
| Efficiency    | **[Single Ease Question](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#the-3-questions-we-ask) per task** <br> <br> + ask participants WHY they rated it that way | *“Overall, this task was…”* <br> *- Extremely difficult* <br> *- Difficult* <br> *- Neither easy nor difficult* <br> *- Easy* <br> *- Extremely easy* <br> *“Why?”*                                       | It's the subjectively perceived task difficulty and we ask it during CMS. Collecting it as part of a usability test gives a predication for a later CMS study. <br><br>We only talk to a small number of participants, which makes it important to understand their reasons for giving a rating, especially in situations where a rating is low. |
|               | **[Adjective rating scale](https://uxpajournal.org/determining-what-individual-sus-scores-mean-adding-an-adjective-rating-scale/) after all tasks are completed** <br> <br> + ask participants WHY they gave that score | *“Overall, I would rate the user-friendliness of this product as:”* <br> *- Worst-imaginable* <br> *- Awful* <br> *- Poor* <br> *- OK* <br> *- Good* <br> *- Excellent* <br> *- Best imaginable* <br> *“Why?”* | It highly correlates to SUS and our aim is to [increase system usability](https://about.gitlab.com/company/strategy/#2-build-on-our-open-core-strength).  <br><br>Because we talk to a small sample size, we can’t just collect the score - we need to know their reason for this score.                                                                       |

    
7. [Run a pilot session to test the usability test](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/writing-usability-testing-script/#3-test-the-test).
8. [Analyze your research data](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/analyzing-research-data/)
    - For each task, synthesize how many users succeeded or failed and why they failed. 
    - For each task, calculate how easy or difficult it was for participants to complete it. Look for patterns on why they gave a score.
    -  Calculate the average score for the [SUS adjective rating scale](https://uxpajournal.org/determining-what-individual-sus-scores-mean-adding-an-adjective-rating-scale/), and look for patterns in why they gave a particular score. 
    - Note down any other interesting observations you had. 
9. [Document your insights in Dovetail](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/documenting-research-findings/). If you have actionable insights, ensure they are also [documented in the GitLab UX Research project](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-insights/#how-to-document-actionable-insights).
10. Decide on the next steps.
    - Any [actionable insights](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-insights/#actionable-insights) require a follow up. Work with your counterparts to determine priority for the identified usability problems. Remember to conduct another usability study to validate your proposed solution.

## How usability testing relates to Category Maturity Scorecards (CM Scorecards)
Usability testing happens before you conduct a [CM Scorecard](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#intro-and-goal). Usability testing helps to identify the majority of problems users encounter, the reasons for these issues, and their impact when using GitLab, so we know what to improve. 

If you run a CM Scorecard without prior usability testing, you will likely identify some of the usability problems users experience. However, the effort and rigor connected with the Category Maturity Scorecard to measure objectively the current maturity of the product doesn’t justify skipping usability testing. 
In addition, during usability testing you have opportunities to engage with participants directly and dive deeper into understanding their behaviors and problems. The Category Maturity Scorecard process does not allow for such interactions as it's designed to capture unbiased metrics and self-reported user sentiment.



