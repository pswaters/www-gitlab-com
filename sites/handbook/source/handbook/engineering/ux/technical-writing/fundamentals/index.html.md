---
layout: handbook-page-toc
title: "Technical Writing Fundamentals"
description: "Get started working with GitLab product documentation with our Technical Writing Fundamentals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

To help documentation contributors from GitLab and the community to document product features, the Technical Writing team developed the Technical Writing Fundamentals course. This course provides direction on grammar, tooling, and topic design. 

## Technical Writing Fundamentals course

The Technical Writing Fundamentals course consists of the following sections, which you should complete in order:

1. [**Pre-class work**](https://developers.google.com/tech-writing/one): Complete the pre-class exercises from this excellent Google training. You can take the Google class if you like, but the following steps cover the  information that is relevant for GitLab.
1. [**Session 1**](https://docs.google.com/presentation/d/1gIqSDoUY7JiV5AEVY12CQS3WtNbN9kXFb52xUsOTfeg/edit?usp=sharing): Covers material from the pre-class work, including grammar and style requirements related to the GitLab Style Guide. _coming soon: video_
1. [**Session 2**](https://docs.google.com/presentation/d/1TCQJ-Bwgj4gByNgOuSHbqB8FLctScW_wq9QD7zNDeQc/edit?usp=sharing): Describes the Concept, Task, Reference, and Troubleshooting (CTRT) topic structure, with a link to examples. _coming soon: video_

## Schedule in-person training

While we emphasize asynchronous training, in-person training for GitLab team members is available on a limited basis. Contact Susan Tacker (`@susantacker` in Slack) to schedule training sessions.
