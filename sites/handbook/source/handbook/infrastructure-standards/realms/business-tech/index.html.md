---
layout: handbook-page-toc
title: "Business Technology Realm"
description: "This infrastructure realm is for resources managed by the IT and Business Technology team."
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Quick links

* [Global infrastructure standards](/handbook/infrastructure-standards/)
* [Global labels and tags](/handbook/infrastructure-standards/labels-tags/)
* [Infrastructure policies](/handbook/infrastructure-standards/policies/)
* [Infrastructure helpdesk](/handbook/infrastructure-standards/helpdesk/)

## Overview

This infrastructure realm is for resources managed by the IT and Business Technology team.

### Access requests

To request access to a group, please see [group access request tutorial](/handbook/infrastructure-standards/tutorials/groups/access-request).

> For email authenticity security reasons, only GitLab issues or Slack messages to owners or counterparts are allowed for infrastructure requests.

### Realm Owners

| Name                 | GitLab.com Handle       | Group Role       | Job Title                                |
|----------------------|-------------------------|------------------|------------------------------------------|
| TBD       |         | Owner                    | TBD              | TBD                                      |
| Mohammed Al Kobaisy  | `malkobaisy`            | Counterpart      | IT Operations System Administrator       |
| Daniel Parker        | `djparker`              | Counterpart      | Sr Integrations Engineer                 |

## Realm labels and tags

The [global labels/tags](/handbook/infrastructure-standards/labels-tags) and [realm labels/tags](/handbook/infrastructure-standards/realms/business-tech/labels-tags) should be applied to each resource.

## Realm Groups

Each infrastructure group has a shared GCP project and/or AWS account for group members.

If a group has not been implemented yet, please contact the realm owner for assistance. After a group is implemented, a separate handbook page is created with usage documentation.

| Group Name (AWS Account/GCP Project Name) | Usage Documentation (Empty cells are not implemented yet)                                                               |
|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| `business-tech-shared-infra`               | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/business-tech-shared-infra)-->            |
| `business-tech-shared-services`            | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/business-tech-shared-services)-->         |
| `business-tech-data`                       | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/business-tech-data)-->                    |
| `business-tech-operations`                 | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/business-tech-operations)-->              |
| `business-tech-systems`                    | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/business-tech-systems)-->                 |
| `mktg-ops-shared-infra`                   | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/mktg-ops-shared-infra)-->                |
| `sales-alliances-shared-infra`            | <!--[Group Docs](/handbook/infrastructure-standards/realms/business-tech/groups/sales-alliances-shared-infra)-->         |

## Usage guidelines

This is a placeholder for the realm owner to provide instructions on best practices and usage guidelines for this infrastructure.
