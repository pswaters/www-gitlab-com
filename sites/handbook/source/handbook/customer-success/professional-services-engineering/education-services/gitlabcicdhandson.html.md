---
layout: handbook-page-toc
title: "GitLab CI/CD Hands On Guide"
description: "This Hands On Guide is designed to walk you through the lab exercises used in the GitLab CI/CD course."
---
# GitLab CI/CD Hands On Guide
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hands On Lab Setup

1. To begin lab, navigate to the sandbox link: <https://www.gitlabdemo.com/invite>  
2. Enter your invitation code in the box and Click **Redeem**. 
3. You will be brought to the link below: 
4. Click **Download Credentials**. 
5. Click the GitLab URL and launch your demo environment. 
6. Enter your credentials and launch the demo environment. 

## GitLab CI/CD Labs
* [Lab 1- Review Example GitLab CI/CD Section](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab1.html)
* [Lab 2- Create a Project and a GitLab-ci.yml File](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab2.html)
* [Lab 3- Create a Basic CI Configuration](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab3.html)
* [Lab 4- Define Pipeline Environments](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab4.html)
* [Lab 5- Add Variable Hierarchy](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab5.html)
* [Lab 6- Define Job Policy Pattern](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab6.html)
* [Lab 7- Add Artifact Hierarchy](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab7.html)
* [Lab 8- Enable GitLab Docker Registry](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab8.html)
* [Lab 9- Enable Security Scanning](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9.html)


## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Demo Cloud](https://gitlabdemo.com/login)
* [GitLab CI/CD Course Description](https://about.gitlab.com/services/education/gitlab-ci/)
* [GitLab CI/CD Specialist Certifcation Details](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab CI/CD - please submit your changes via Merge Request!

