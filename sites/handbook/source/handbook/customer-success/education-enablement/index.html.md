---
layout: handbook-page-toc
title: "Customer Success Education and Enablement"
description: "As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview of Education and Enablement

As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed. Although we aggregated and/or developed these resources for Customer Success team members, they are generic enough that all team members and partners can benefit from the education and enablement resources that we have published here.

## Educational Tiers

There is a lot of depth to the GitLab product and the industry knowledge that you'll need to know (eventually). To help you level up, the resources on this page have been broken up into educational tiers.

* **Foundations (101 Level)** - This provides an overview that lets you get a summary about a topic in less than 30 minutes to be able to have a high-level conversation about it.
* **Associate (150 to 200 Level)** - This provides a working knowledge of the depth of a topic and associated features. This provides enough information to know that a feature exists and a "good enough" understanding of how it works until you find yourself needing to focus on a topic for a more complex demo or implementation.
* **Professional (300-400 Level)** - This provides the subject matter expert level materials for topics of interest. If you become proficient in a topic, you are encouraged to contribute resources (videos, demos, post-mortem write-ups, documentation pages, repositories, etc.) to this tier since this is crowd sourced from team members and not formally developed as part of our education and enablement program.

## Overview of GitLab Feature and Use Cases

The GitLab product is organized based on the DevOps stages.

### DevOps Stages

If you're new to GitLab and the concept of "stages" are new to you, simply think of it as a category of related features that a customer finds value in with a **technical** narrative. The stages are organized in an infinite loop diagram to show the software development lifecycle from planning to development to release, with additional value for deployed applications with security and monitoring features. You can see a high-level overview of each stage on the [marketing page](https://about.gitlab.com/stages-devops-lifecycle/). Be sure to click the `Learn More` link for each stage to see more about the features of that stage.

One of the most helpful resources for understanding what GitLab does, the features in each stage, and how well the feature works is our [Category maturity](https://about.gitlab.com/direction/maturity/) chart. This may also be informally referred to as our "lovable feature chart".

If you understand what the stages are and want to dive deeper, you can look at the [product categories handbook page](https://about.gitlab.com/handbook/product/categories/) for a deep-dive on each of the stages, specifically the [hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy) and the [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages).

For the purposes of education and enablement, we focus on the DevOps stages that the customer sees. There are additional stages that help GitLab as a business succeed, such as the [Growth stage](https://about.gitlab.com/handbook/product/categories/#growth-stage), [Fulfillment stage](https://about.gitlab.com/handbook/product/categories/#fulfillment-stage), [Enablement stage](https://about.gitlab.com/handbook/product/categories/#enablement-stage), and [Single Engineer Groups stage](https://about.gitlab.com/handbook/product/categories/#single-engineer-groups-section). Although these are not necessarily DevOps stages, the Engineering and Product departments use the stage terminology for consistency. **You do not need to spend time learning about these stages and can search for this information later if the need arises.**

### Use Cases

The GitLab use cases help define the categories of related features with a **business value** narrative. In other words, why are customers buying GitLab and what do they think that we offer. You can read more about each of our use cases by navigating to the Product navigation menu at the top of this page or from anywhere on [about.gitlab.com](https://about.gitlab.com/).

* [Source Code Management](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/)
* [Continuous Integration and Deployment/Delivery (CI/CD)](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
* [Auto DevOps](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/)
* [Security and DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops/)
* [Agile Delivery](https://about.gitlab.com/solutions/agile-delivery/)
* [Value Stream Management](https://about.gitlab.com/solutions/value-stream-management/)
* [GitOps and Infrastructure-as-Code](https://about.gitlab.com/solutions/gitops/)

There are also resources available to cater to the size of customer and their needs.

* [Small Business](https://about.gitlab.com/small-business/)
* [Enterprise](https://about.gitlab.com/enterprise/)

## GitLab Product Topics

To align our education and enablement with our product, each of the topics below are grouped based on how GitLab product management and engineering defines it.

<table>
<tr>
<th>Topic</th>
<th>Foundations</th>
<th>Associate</th>
</tr>
<!-- DevOps Sections -->
<tr>
<th><a name="devops-sections">DevOps Sections</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/direction/dev/">Dev Section Direction and Roadmap</a></li>
    <li><a href="https://about.gitlab.com/direction/ops/">Ops Section Direction and Roadmap</a></li>
    <li><a href="https://about.gitlab.com/direction/security/">Security Section Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
</td>
</tr>
<!-- Manage Stage -->
<tr>
<th><a name="manage-stage">Manage Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/manage/">Manage Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/dev/#manage">Manage Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://docs.gitlab.com/ee/user/group/subgroups/">Subgroups Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/group/insights/">Insights Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/solutions/value-stream-management/">Value Stream Management Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/administration/audit_events.html">Audit Events Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html">DevOps Reports Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/analytics/code_review_analytics/">Code Analytics Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/administration/compliance.html">Compliance Management Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/administration/audit_reports.html">Audit Reports Feature Docs</a></li>
    </ul>
</td>
</tr>
<!-- Plan Stage -->
<tr>
<th><a name="plan-stage">Plan Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/plan/">Plan Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/dev/#plan">Plan Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://docs.gitlab.com/ee/user/project/issues/">Issue Tracking Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/solutions/time-tracking/">Time Tracking Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/product/issueboard/">Boards Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/product/epics/">Epics Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/product/roadmaps/">Roadmaps Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/product/service-desk/">Service Desk Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/requirements/">Requirements Management Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/test_cases/index.html">Quality Management Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/issues/design_management.html">Design Management Feature Docs</a></li>
    </ul>
</td>
</tr>
<!-- Create Stage -->
<tr>
<th><a name="create-stage">Create Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/create/">Create Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/dev/#create">Create Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/source-code-management/">Source Code Management Feature Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/create/source_code_management/">Source Code Management Direction and Roadmap</a></li>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/code-review/">Code Review Feature Overview with 30+ Features</a>
    <ul>
    <li><code>Free</code><a href="https://about.gitlab.com/solutions/jira/">JIRA Integration Solution Overview</a></li>
    <li><code>Free</code><a href="https://docs.gitlab.com/ee/user/project/integrations/jira.html">JIRA Issues Integration Feature Docs</a></li>
    <li><code>Free</code><a href="https://docs.gitlab.com/ee/integration/jira/index.html">JIRA Development Panel Integration Feature Docs</a></li>
    <li><code>Premium</code><a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html">Multiple approvers in code review Feature Docs</a></li>
    <li><code>Premium</code><a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html">Approval rules for code review Feature Docs</a></li>
    <li><code>Premium</code><a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_dependencies.html">Merge request dependencies Feature Docs</a></li>
    <li><code>Premium</code><a href="https://docs.gitlab.com/ee/user/project/code_owners.html">Code Owners Feature Docs</a></li>
    </ul>
    </li>
    <li><a href="https://docs.gitlab.com/ee/user/project/wiki/">Wiki Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/static_site_editor/">Static Site Editor Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/web_ide/index.html">Web IDE Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/web_ide/index.html#live-preview">Live Preview Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/snippets.html">Snippets Feature Docs</a></li>
    <li><a href="https://gitlab.com/gitlab-org/gitaly">Gitaly Source Code Project</a></li>
    </ul>
</td>
</tr>
<!-- Verify Stage -->
<tr>
<th><a name="verify-stage">Verify Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/verify/">Verify Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/ops/#verify">Verify Stage Direction and Roadmap</a></li>
    <li><a href="https://youtube.com/playlist?list=PL05JrBw4t0Ko-mJZLo2uF3aQQuBfBaSKB">CI YouTube Playlist</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://about.gitlab.com/product/continuous-integration/">Continuous Integration (CI) Feature Overview</a><br />
    <ul>
    <li><a href="https://docs.gitlab.com/ee/ci/">CI/CD Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/introduction/">CI/CD Concepts Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/pipelines/">CI/CD Pipeline Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/variables/README.html">CI/CD Variables Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/environments/">CI/CD Environments and Deployments Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/runners/README.html">CI/CD with Runners Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/topics/autodevops/">Auto DevOps Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html">CI/CD configuration with <code>.gitlab-ci.yml</code> Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/yaml/README.html">CI/CD authoring YAML reference for <code>.gitlab-ci.yml</code> Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/examples/README.html">CI/CD Implementation Examples Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/troubleshooting.html">CI/CD Troubleshooting Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/migration/circleci.html">Migrate from CircleCI Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/migration/jenkins.html">Migrate from Jenkins Docs</a></li>
    </ul>
    </li>
    <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html">Code Quality Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/unit_test_reports.html">Code Testing and Coverage Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html">Load Testing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html">Browser Performance Testing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter">Usability Testing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html">Accessibility Testing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/">Merge Trains Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="package-stage">Package Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/package/">Package Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/package/">Package Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://docs.gitlab.com/ee/user/packages/package_registry/">Package Registry Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/packages/container_registry/">Container Registry Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts">Helm Chart Registry Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/packages/dependency_proxy/">Dependency Proxy Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/direction/package/#dependency-firewall">Dependency Firewall Direction and Roadmap</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/releases/#release-evidence">Release Evidence Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/topics/git/lfs/index.html">Git LFS - Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="release-stage">Release Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/release/">Release Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/release">Release Stage Direction and Roadmap</a></li>
    <li><a href="https://about.gitlab.com/resources/ebook-single-app-cicd/">CI/CD eBook</a></li>
    <li><a href="https://about.gitlab.com/blog/2017/03/13/ci-cd-demo/">CI/CD YouTube Demo Overview</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://about.gitlab.com/product/continuous-delivery/">Continuous Delivery Feature Overview</a>
    <ul>
    <li><a href="#verify-stage">See verify stage for full list of CI/CD resources</a></li>
    </ul>
    </li>
    <li><a href="https://about.gitlab.com/product/pages/">Pages Feature Overview</a>
    <ul>
    <li><a href="https://about.gitlab.com/direction/release/pages">Pages Feature Direction and Roadmap</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/pages/">Pages Feature Docs</a></li>
    </ul>
    </li>
    <li><a href="https://about.gitlab.com/product/review-apps/">Review Apps Feature Overview</a>
    <ul>
    <li><a href="https://www.youtube.com/watch?v=CteZol_7pxo&feature=youtu.be">YouTube Webcast Feature Overview</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-org/-/epics/495">Review Apps Direction and Roadmap</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/review_apps/">Review Apps Feature Docs</a></li>
    </ul>
    </li>
    <li><a href="https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium">Advanced Deployments Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/feature_flags.html">Feature Flags Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/releases/">Release Orchestration Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="configure-stage">Configure Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/configure/">Configure Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/configure/">Configure Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <!--<li><a href="#"></a></li>-->
    <li><a href="https://about.gitlab.com/product/auto-devops/">Auto DevOps Feature Overview</a></li>
    <li><a href="https://docs.gitlab.com/ee/topics/autodevops/">Auto DevOps Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/solutions/kubernetes/">Kubernetes Management Feature Overview</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/variables/">Secrets Management CI Variable Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/ci/chatops/">ChatOps Feature Docs</a></li>
    <li><a href="https://about.gitlab.com/product/serverless/">Serverless Feature Overview</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/infrastructure/">Infrastructure-as-Code Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/clusters/cost_management.html">Cluster Cost Management Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="monitor-stage">Monitor Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/monitor/">Monitor Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/monitor/">Monitor Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://docs.gitlab.com/ee/user/project/clusters/runbooks/">Runbooks Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/metrics/">Metrics Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/incident_management/">Incident Management Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#kubernetes-pod-logs">Logging Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/tracing.html">Tracing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/error_tracking.html">Error Tracking Feature Docs</a></li>
    <li><a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8338">Synthetic Monitoring Direction and Roadmap</a></li>
    <li><a href="https://docs.gitlab.com/ee/operations/product_analytics.html">Product Analytics Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="secure-stage">Secure Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/secure/">Secure Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/secure/">Secure Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <!--<li><a href="#"></a></li>-->
    <li><a href="https://docs.gitlab.com/ee/user/application_security/sast/">Static Application Security Testing (SAST) Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/dast/">Dynamic Application Security Testing (DAST) Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/">Fuzz Testing Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/dependency_scanning/">Dependency Scanning Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html">License Compliance Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/secret_detection/">Secret Detection Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/security_dashboard/">Vulnerability Management Feature Docs</a></li>
    </ul>
</td>
</tr>
<tr>
<th><a name="protect-stage">Protect Stage</a></th>
<td>
    <!-- Foundations -->
    <ul>
    <li><a href="https://about.gitlab.com/stages-devops-lifecycle/protect/">Protect Stage Overview</a></li>
    <li><a href="https://about.gitlab.com/direction/protect/">Protect Stage Direction and Roadmap</a></li>
    </ul>
</td>
<td>
    <!-- Associate -->
    <ul>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/container_scanning/">Container Scanning Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/application_security/threat_monitoring/#configuring-network-policy-alerts">Security Orchestration Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/clusters/protect/container_host_security/index.html">Container Host Security Feature Docs</a></li>
    <li><a href="https://docs.gitlab.com/ee/user/project/clusters/protect/container_network_security/index.html">Container Network Security Feature Docs</a></li>
    </ul>
</td>
</tr>
</table>

## GitLab Training and Certification Program

### Professional Services Classes

Individual GitLab team members can request to audit a customer-facing training sessions delivered by Professional Services. Internal GitLab team leads can request training sessions delivered by Professional Services for their teams.

To learn more, see the [Working with Professional Services handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/working-with/#requesting-training-for-gitlab-team-members).

### GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers.

To learn more, see the [GitLab Technical Certifications handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/).

## Industry Topics

### Cloud Infrastructure

TODO AWS, GCP, DigitalOcean

### Container Technologies

TODO Kubernetes, Openshift, Docker

### Infrastructure-as-Code

<table>
<tr>
<th>Topic</th>
<th>Foundations</th>
<th>Associate</th>
<th>Professional</th>
</tr>
<!-- HashiCorp Terraform -->
<tr>
<th><a name="hashicorp-terraform">HashiCorp Terraform</a></th>
<td>
<ul>
<li><a href="https://learn.hashicorp.com/terraform">Hashicorp Learn Tutorials</a></li>
<li><a href="https://docs.gitlab.com/ee/user/infrastructure/">GitLab Terraform Integration Docs</a></li>
</ul>
</td>
<td>
<ul>
<li><a href="https://www.terraform.io/docs/index.html">Terraform Docs</a></li>
<li><a href="https://registry.terraform.io/providers/hashicorp/aws/latest/docs">Terraform AWS Provider Docs</a></li>
<li><a href="https://registry.terraform.io/providers/hashicorp/google/latest/docs">Terraform GCP Provider Docs</a></li>
<li><a href="https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs">Terraform GitLab Provider Docs</a></li>
<li><a href="https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started">GitLab Sandbox Cloud</a></li>
<li><a href="https://gitlab.com/gitlab-com/demo-systems/terraform-modules">GitLab Demo Systems Terraform Modules</a></li>
</ul>
</td>
<td>
<ul>
<li><a href="https://www.hashicorp.com/certification/terraform-associate">Terraform Associate Certification</a></li>
</ul>
</td>
</tr>
<!-- HashiCorp Vault -->
<tr>
<th><a name="hashicorp-vault">HashiCorp Vault</a></th>
<td>
<ul>
<li><a href="https://learn.hashicorp.com/terraform">Hashicorp Learn Tutorials</a></li>
</ul>
</td>
<td>TODO</td>
<td>
<ul>
<li><a href="https://www.hashicorp.com/certification/vault-associate">Vault Associate Certification</a></li>
</ul>
</td>
</tr>
<!-- HashiCorp Consul -->
<tr>
<th><a name="hashicorp-consul">HashiCorp Consul</a></th>
<td>
<ul>
<li><a href="https://learn.hashicorp.com/consule">Hashicorp Learn Tutorials</a></li>
</ul>
</td>
<td>TODO</td>
<td>
<ul>
<li><a href="https://www.hashicorp.com/certification/consul-associate">Consul Associate Certification</a></li>
</ul>
</td>
</tr>
</table>

TODO Packer, Ansible, Chef, Puppet

### Web Application Security

TODO OWASP, Tenable, etc.

## Demo Readiness

Getting started demos for baseline proficiency. Learn more on the [Demo Readiness](/handbook/customer-success/solutions-architects/demonstrations/#demo-readiness) page

## Demo Library

TODO Table of all sample projects and video walk through demos

## Enablement Events and Async Recordings

### Customer Success Skills Exchange

The Field Enablement Team hosts a 50 min weekly webinar focused on topics relevant to the Customer Success Team. To learn more, view the upcoming schedule, or check out a recording go to the [CS Skills Exchange handbook page](/handbook/sales/training/customer-success-skills-exchange/).

### Sales Enablement Level Up

The Field Enablement Team hosts a weekly 30 minute webinar focused on topics relevant to the entire Field Team. To learn more, view the upcoming scheudle, or check out a recording go to the [Sales Level Up Webinar handbook page](/handbook/sales/training/sales-enablement-sessions/).

### Command of Message & MEDDPPICC

GitLab uses ForceManagement's Command of Message and MEDDPPICC as our sales methodology. You can familiarize yourself with the kep components and messaging on [Command of Message handbook page](/handbook/sales/command-of-the-message/).

## Helpful Handbook Pages and Internal Google Drive Links

TODO List of bookmarks

## Additional Resources

- [Field Enablement Handbook Page](/handbook/sales/field-operations/field-enablement/)
- [Sales Training](/handbook/sales/training/)
- [O'Reilly](https://learning.oreilly.com/home/). Contact your manager for a license.
- [Communities of Practice](/handbook/customer-success/initiatives/communities-of-practice)
- [Learn at GitLab](https://about.gitlab.com/learn/)
