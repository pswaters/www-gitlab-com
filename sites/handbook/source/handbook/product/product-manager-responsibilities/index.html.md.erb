---
layout: handbook-page-toc
title: Product Manager Responsibilities
description: "This page highlights the focus of a product managers job, reponsiblities and required tasks at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

It has never been more [popular](https://medium.com/agileinsider/incredible-growth-in-demand-for-product-managers-in-the-us-but-not-necessarily-in-the-places-youd-936fec5c1932) to be a product manager,
but the responsibilities of a product manager can vary greatly between organizations and even within an organization.

## Core PM Responsibilities

As a product manager at GitLab, you are primarily responsible for:

1. Understanding and communicating the problems of our users and customers
1. Contributing towards building products loved by our users and customers
1. Ensuring our product is viable for GitLab

In addition, as a PM, you also play a critical role in the regular development and operating candence of GitLab. There are a few specific required tasks that the PMs are directly responsible for in [Core PM Tasks](#core-pm-tasks).

### How does a PM understand and communicate the problems of our users and customers?

PMs should spend a significant amount of time understanding the problems our users face. Deeply understanding the problem is the foundation for all other activities PMs take on; understanding the problem enables the PM to define the vision and prioritize effectively.

In order for a PM to deeply understand the problems our users are facing and determine which are the important problems to solve, they can do the following:

- Have a deep knowledge of each category's problem space
- Regularly talk with users and customers
- Communicate with users on issues and epics
- Work with Pre-Sales and Post-Sales to connect with customers and prospects
- Uncover insights through UX Research
- Use other [sensing mechanisms](/handbook/product/product-processes/#sensing-mechanisms). It is through user interactions that we can begin to understand what problems our users are facing and identify how we could make product improvements to help eliminate the pain points. These user interactions can present themselves in many different ways.

### How does a PM contribute to building lovable products?

Producing a lovable product requires more than a deep understanding of the problem. At GitLab, we build lovable products by adhering to our [values](/handbook/values/). PMs are expected to be the ambassador of the GitLab values by:

- Focusing on [results](/handbook/values/#results).
- Embracing [iteration](/handbook/values/#iteration). This is the secret to GitLab moving fast, as we get constant feedback and maintain forward momentum toward GitLab's huge [vision](/direction/#vision). PMs play a large role in unlocking iteration as a competency.
- Being [transparent](/handbook/values/#transparency). This will enable both our development group to contribute and just as important, enable the wider GitLab community to contribute.
- Being [efficient](/handbook/values/#efficiency). PMs should advocate for the boring solution, optimize for the right group, be the manager of one, always write things down, so they can help their groups also be efficient.
- Being [collaborative](/handbook/values/#collaboration). PMs ultimately don't ship anything on their own. PMs need to be a great teammate so that the development group can produce great work.
- Being someone who helps make GitLab a work place of [diversity, inclusion, and belonging](/handbook/values/#diversity-inclusion), so that everyone can come to GitLab and do their best work.

### How does a PM ensure business viability for our product?

It is not sufficient to just know the problems. It is also insufficient to have a solution to the problem that our customers love. PMs also need to ensure that the solution is viable for GitLab.

- PMs participate in and follow the [product development flow](/handbook/product-development-flow/) so that their development group can consistently release features [every month](/releases/).
- PMs [determine the tiers of features](https://about.gitlab.com/handbook/ceo/pricing/#departments)
- PMs ensure that issues that impact GitLab are appropriately [prioritized](https://about.gitlab.com/handbook/product/product-processes/#prioritization)
- PMs interface with marketing and sales to promote and enable the sale of the product.

## Core PM Tasks

In an ongoing effort to help product managers be more efficient and maximize time spent on high value activities in alignment with [core PM responsibilities](#core-pm-responsibilities), below is a list to clarify what actions are **required** for product managers. Over time, our goal is to eliminate and automate any actions product managers currently have to do (due to tactical needs) that do not directly link to the core PM responsibilities defined above. 

 | Task                              | Description                                                                               |
| Maintain and update Direction pages | Direction pages communicate our vision and plan externally and internally. |
| Review PIs and update PI pages | Product Indicators are how we know whether or not we are making the right investments or measuring the right thing. PMs should be intimately knowlegeable with the PIs in their domain. |
| Release planning for every milestone | PMs are the DRIs for planning and prioritizing the work for their respective development group. Some of the output for release planning can be planning issues and kick-off videos. |
| Create release post content | The release post is the way to broadcast what features have been released. PMs are responsible to ensure release post item MRs are created, reviews are complete and posted.
| Drive alignment with stable counterparts via OKRs |  PMs play a role in facilitating alignment by ensuring individual functional groups' objectives are appropriately prioritized within their development group. |
|Connect user and customer insights with product prioritization |  Customer interviewing, collecting insights about customer adoption, pain points, and reprioritizing "What's Next". PMs are responsible for continuously refining the most important feature, bugs, and tech debt to ensure relevance to the market, install base, and expansion. |
| Triage new issues (features, bugs, security vulnerabilities, etc) | PMs are responsible for prioritizing the most important issues to work on. |
| Determine pricing tier for features | [The CEO is responsible for pricing, but PMs are responsible for determining which plan features belong to.](https://about.gitlab.com/company/pricing/#departments) |

## What is a PM not responsible for?

To deeply understand the problems to solve, to build a lovable product, and to ensure the product is viable is not easy. It requires the Product Manager to be tenacious, influential, technically savvy, entrepreneurial, strategic, and many more adjectives. To support Product Managers, many processes and activities have been created. However, those activities are not always necessary if they do not contribute to the [core PM responsibilities](#core-pm-responsibilities) or are not part of the [core PM tasks](#core-pm-tasks).

Product Managers are not responsible for: 
- Technical decisions on how the product is built or architected. Engineers are the DRIs
- Design and UX. UX designers are the DRIs
- A team of engineers: PMs will take the lead in decisions about the product, but not manage the people implementing it
- Capacity planning: PM will define priorities, but the Engineering Manager evaluates the amount of work possible
- Shipping in time: PM will work in a group, but the group is responsible for shipping in time, not you
- [Negotiation of Community Contributions](https://about.gitlab.com/handbook/product/product-processes/#gitlab-pms-arent-the-arbiters-of-community-contributions) 
