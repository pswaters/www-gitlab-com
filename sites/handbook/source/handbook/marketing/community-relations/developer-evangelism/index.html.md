---
layout: handbook-page-toc
title: "Developer Evangelism"
description: "We build GitLab's technical brand with deep, meaningful conversations on engineering topics relevant to our community."

---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Mission 

To support, grow, and engage the GitLab community through collaboration, content, and conversations.

## <i class="far fa-newspaper" id="biz-tech-icons"></i> Strategy
Developer relations and developer evangelism is an ever-expanding space that can seem saturated with marketing aimed at developers.  This type of developer marketing can have the inverse effect - causing developers and other technologists to be less likely to interact with companies and communities that seem disingenuous. 

For this reason, we choose to have a team with credibility from real-world engineering team experience.  Additionally, we enable others to tell **their** authentic stories - engineers, open source community members, and GitLab's diverse set of customers.  For those reasons, we chose the title of [`Evangelist`](https://www.merriam-webster.com/dictionary/evangelist) as defined as "an enthusiastic advocate."

When differentiating this approach from traditional developer relations programs or groups, we want to focus on areas that are often overlooked, including:
* DevOps
  - We want our work to speak to not only developers but all team members involved in the DevOps lifecycle to deliver working code to production: Product Managers, software engineers, designers, test engineers, security engineers, operations engineers, and SREs
* Enterprise
  - Developers and DevOps professionals in the enterprise have special constraints and needs.  Often these are glossed over with easy “throw out your architecture and use this new shiny thing” - we won’t do that, we’ll acknowledge real-world challenges, legacy code and enterprise constraints and help people solve those problems as well. When applicable, we switch roles into consulting and support.

## What we do

The GitLab developer evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time Developer Evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem.
1. **Ecosystem engagement:** Establish GitLab's technical thought leadership by building GitLab's influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [Open Source Program handbook page](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships).
1. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize the value of, and measure the impact of content created for thought leadership and ecosystem engagement.

### Social media

We build our thought leadership on social media. See [Developer Evangelism on Social Media](/handbook/marketing/community-relations/developer-evangelism/social-media/) to learn more about our strategies and become an evangelist yourself.

### Content creation 

We build out content to help educate developers around best practices related to DevOps, GitLab, remote work, and other topics where we have expertise. Content includes presentations, demos, workshops, blog posts, and media engagements. 

We maintain a [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ) with our talks, workshops and community engagements.

#### UTMs for URL tagging and tracking

When contributing to articles on external sites, please request that the author or publisher include a [UTM code](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#utms-for-url-tagging-and-tracking) linking to the GitLab marketing website (i.e. `about.gitlab.com`) pages that are referenced in the article or story. 

The UTM codes to use are:

- utm_source: An identifier for the website where content is published
- utm_medium: e.g.: `social`, `presentation`, `email`, `partners`, `other` or any other medium specified in the [UTM Tracking sheet](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=3). To use any source not listed, the [Digital Marketing team](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/) needs to be informed.
- utm_campaign: `de-external-blogposts` or any other periodic campaigns.

Any UTM links created should be added to the [UTM Tracking sheet](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) for GitLab-wide tracking and coordination. Review the [Digital Marketing handbook page](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#utms-for-url-tagging-and-tracking) for more information on UTM tracking.

#### Blog Post PostType

We write across diverse platforms, but a primary destination for our writings is the [GitLab Blog](/blog/), where all our blogposts include the `dev-evangelism` [postType](/handbook/marketing/blog/#post-type) in their [frontmatter](/handbook/marketing/blog/#frontmatter) for proper tracking.

#### Content Reuse

The Developer Evangelism team creates alot of content that can be reused for any campaigns. All contents and activities the team participates in are added to the team's [activity tracking sheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1818319548). You can search for relevant content and contact the author or the team on slack in the #dev-evangelism channel for clarification where needed.

### Presentations and speaking

Speaking at events is an essential path for our team to connect with the wider GitLab community and the tech community at large. In addition to our own speaking engagements, we are happy to provide support to team members and members of the wider community on their presentations.

### CFPs

Our Developer Evangelists directly contribute to the wider community by speaking at conferences themselves. We also [support and manage responses to CFPs](/handbook/marketing/community-relations/developer-evangelism/cfps/) for team members across GitLab through our issue boards.

### Speaker prep

To have someone from our team review a presentation, please connect with a member of our team via [Slack](https://gitlab.slack.com/archives/CMELFQS4B) or Twitter. 

#### Speakers Bureau

People who regularly speak about GitLab may be interested in joining the [GitLab Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).

### Community engagement

Part of our role is to respond to engage with and answer questions from community members. We do this organically on social media, when prompted by our social media team or other GitLab team members, and by [monitoring GitLab and other selected keywords on Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/).

#### Community response

Given the Developer Evangelism team's understanding of our community and broad knowledge of GitLab, we regularly engage in responding to situations that require intervention to address urgent and important concerns of our community members. We have a documented process how we [manage these situations](/handbook/marketing/community-relations/developer-evangelism/community-response/).

### Release evangelism

Developer Evangelists should always be prepared to promote our [monthly release](/handbook/marketing/community-relations/developer-evangelism/social-media/#release-evangelism) and engage in [community response on release days](/handbook/marketing/community-relations/developer-evangelism/hacker-news/#release-days) given the historical performance of release posts on Hacker News. 

### Projects

Our team maintains many projects to help show off technical concepts, engage with communities, provide examples of using GitLab with other technologies, and automate our team processes. See [Developer Evangelism Projects](/handbook/marketing/community-relations/developer-evangelism/projects/) for a list of all of those projects.

### OSS Contributions

We actively contribute to OSS projects and share our technical expertise. You can learn more about our ideas and visions in our [OSS contributions](/handbook/marketing/community-relations/developer-evangelism/oss-contributions/) handbook page.

### Metrics Collection and Analysis

Measuring what we do is very important to understand our impact and how we are able to reach our OKRs. A key metric is the Developer Evangelists' cumulative Twitter impressions. [Learn more](/handbook/marketing/community-relations/developer-evangelism/metrics/) about the our tools, data collection and how to access the data sources for integrations.

## How we work 

### Issue tracker

We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). We own the label `dev-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups.

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism).

### Issue labels

Using the [dev-evangelism](https://gitlab.com/groups/gitlab-com/-/labels?search=dev-evangelism) label on an issue means we are working on it or participating in the ongoing conversation. Team members are subscribed to issue/MR updates for this label.

The `dev-evangelism` label is accompanied by some labels we use to organize our work.

<i class="fas fa-info-circle" style="color: rgb(49, 112, 143)
;"></i>You only need to use the `dev-evangelism` label on issues requiring the attention of the Developer Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.

{: .alert .alert-info}

#### General labels

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-DueSoon` | This is used to monitor DE issues that are due soon |
| `DE-Peer-Review` | Feedback is needed on the issue from DE team members |
| `DE-Ops` | Used to label issues related to the Developer Evangelism `Ops in DevOps` theme |
| `DE-Dev` | Used to label issues related to the Developer Evangelism `Dev in DevOps` theme |
| `DE-k8s` | Used to label issues related to the Developer Evangelism `Kubernetes` theme |

### Issue management

The team creates issues for iteration, team discussions, and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| -------------- | ----------- |
| `DE-Process::Open` | Process related issues that are still being discussed or worked |
| `DE-Process::Pending` | Process related issues on hold due to an external factor |
| `DE-Process::Done` | Completed Process issues |
| `DE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

## Request support from Developer Evangelism

[Request DE Support](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request){:.btn .btn-purple}

### Developer Evangelism request budgets

In order to prevent burnout, prioritize requests appropriately, and ensure we successfully deliver on the requests to which we commit, our team has created budgets for our internal stakeholders. These budgets encourage team members to prioritize their requests, ensuring our team addresses the highest priority needs for GitLab. 

These request types fall into the following categories: 
1. Event requests
1. CFP requests
1. Content requests 

Ongoing activities including team-driven content creation and speaking oppportunities that supports our goals and OKRs, release support, and social media monitoring, including Hacker News, do not count towards any team budgets. 

#### Event requests

Event requests include both event attendance (ex: attending client meetings, event staffing, attending dinners or social events, monitoring events for news) and speaking engagements at events such as demos and presentations. 

#### CFP (Call for Proposals) requests

CFP requests include any request for a developer evangelist to submit a proposal for an event or media opportunity or support a fellow team member in submitting for an open CFP. 

See [Requesting a Developer Evangelist to submit a CFP](/handbook/marketing/community-relations/developer-evangelism/cfps/) to request a Developer Evangelist to submit to a CFP for a corporate, field, or partner event.

#### Content requests

Content requests include blog post, podcasts, media interviews, or any request that involves engaging a developer evangelist in a media opportunity. 

#### Scoring requests 

| Request Type | New / Existing Content | Budget score |
| ------------ | ---------------------- | ------------ |
| Event        | New                    | 3            |
| Event        | Existing / No content  | 1            |
| CFP          | New                    | 2            |
| CFP          | Existing               | 1            |
| Content      | New                    | 2            |
| Content      | Existing               | 1            |

Each team listed below is allocated 15 points per quarter for requests: 

| Team                       | Team Label  |
|----------------------------|------------------|
| Corporate Events           | `de-corpevents`  |
| Corporate Communications   | `de-corpcomms`   |
| Community Relations        | `de-community`   |
| Growth Marketing           | `de-growthmktg`  |
| Field Marketing / ABM      | `de-fieldmktg`   |
| Sales / SDRs               | `de-sales`       |
| Alliances                  | `de-alliances`   |

If your team is not listed above, we will handle your request based on our availablity.  

### Managing requests

This process covers any content request, Webcast, Interview, Meetup, etc. The process involves the following:

- Requestors should assign a label that identifies their team and a weight correlating with their budget score to allow us to track each team's budget consumption. 
- A member of the Developer Evangelism team will triage the issue and provide all necessary details and directions
- The necessary labels are applied to the issue as actions are taken on the request
- Once the request is complete, the issue is assigned back to the requestor to provide the necessary metrics generated as a result of the before it is closed.

The developer evangelism team will apply the parent `dev-evangelism` label, then transition the issues through different stages using the scoped labels below:

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-Content::new-request` | New content request |
| `DE-Content::draft` | A member of the TE team is working on the content |
| `DE-Content::review` | Content Author is seeking review from other team memebers |
| `DE-Content::published` | Content has been published |
| `DE-Content::Repurposing` | Content is being repurposed on other platforms or mediums |
| `DE-Content::cancelled` | Content is no longer needed, and no one is working on it |

``` plantuml
start
: dev-evangelism, DE-Content::new-request;
if (Content Request Cancelled at any stage) then (yes)
     :dev-evangelism, DE-Content::cancelled;
else
    : dev-evangelism, DE-Content::draft;
    : dev-evangelism, DE-Content::review;
    : dev-evangelism, DE-Content::published;
    : dev-evangelism, DE-Content::Repurposing;
endif

stop
```

#### Tracking impressions

Upon the completion of a request, the assigned Developer Evangelism team member will reassign the issue back to the requestor. The issue should only be closed after the impressions generated by the request are added to the [DE Impression Tracker spreadsheet](https://docs.google.com/spreadsheets/d/10E_TagnV6xgjHorWPTpMnO1Qk33lPR9HkGHOJfa0ENM/edit#gid=1283634798). [Learn more](/handbook/marketing/community-relations/developer-evangelism/cfps/), which we use to collect and track impressions and other metrics.

### Find us on Slack

GitLab team members can also reach us at any time on the [#developer-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.

### Important bookmarks

- CFP
    - [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    - [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
- [Create Developer Evangelist Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request)
- [Events Spreadsheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
- [Events Calendar](#our-events-list)
- [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
- [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
- [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
- Team Issue Boards
    - [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism)
    - [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name%5B%5D=DE-CFP)
    - [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name%5B%5D=dev-evangelism)
    - [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name%5B%5D=dev-evangelism)
- [Team Activity Tracking sheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1818319548)

#### Templates

- [Action Template for Announcement Responses](https://docs.google.com/document/d/1Dhe2hFFZCDRK6eLhrfFqa-iz0bFX8gTZ4keoHo5KrtM/edit) (Internal)

## Team members and focus areas

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

1. [Abubakar Siddiq Ango](/company/team/#abuango) - Developer Evangelism Program Manager
    - DevSecOps with a focus on the Cloud Native Ecosystem
        - Kubernetes
        - CI/CD
    - Program management
        - Running the CFP process for conferences
        - Organizing Developer evangelism team's content creation and repurposing efforts
    - Language skills: English, Yoruba, Hausa
1. [Brendan O'Leary](/company/team/#brendan) - Senior Developer Evangelist
    - DevSecOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
1. [Michael Friedrich](/company/team/#dnsmichi) - Developer Evangelist
    - DevSecOps with a focus on the SRE, Ops and Sec engineers' perspective
        - CI/CD
        - Observability and Monitoring
        - Infrastructure as Code and Security 
    - Language skills: English, German, Austrian
1. [John Coghlan](/company/team/#john-coghlan) - Manager, Developer Evangelism
    - Developer Evangelism
1. [Jamie Rachel - Evangelist Program Manager](/company/team/#Jrachel1)
    - [Meetups](/community/meetups/) 
    - [Heroes](/community/heroes/)

### Stable counterparts 

Inspired by GitLab's [collaboration value](/handbook/values/#collaboration), the Developer Evangelism team has chosen to align ourselves as [stable counterparts](/handbook/leadership/#stable-counterparts) with divisions outside of Marketing. The alignment is as follows: 

- Alliances: [Abubakar Siddiq Ango](/company/team/#abuango)
- Engineering and Product: [Michael Friedrich](/company/team/#dnsmichi)
- Sales: [Brendan O'Leary](/company/team/#brendan)

As stable counterparts, Developer Evangelists are expected to actively engage with the divisions to identify collaboration opportunities and act as the primary point of contact for requests for DE support from these divisions.


## Team Calendar

The Developer Evangelism Team calendar contains team member speaking engagements, important conferences, CFP timelines, and other important dates.

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
Developer Evangelists should add new speaking engagements to this calendar as they are scheduled. The Developer Evangelist Program Manager will manage team-wide events such as industry conferences and their CFP timelines. 
## Learn more about Developer Evangelism as a practice

A good overview with specific area definitions can be found in the [DevRel Notebook](https://github.com/konradsopala/devrel-notebook). 

We engage with Developer advocacy, relations and evangelism friends on social media:

- [Twitter list: Dev Avocados](https://twitter.com/i/lists/1012393598262874112/members) by Quintessence Anx (DevRel Collective founder)
- [Twitter list: DevRel](https://twitter.com/i/lists/1288789359865606145/members) by Michael
- [DevRel Contacts](https://docs.google.com/document/d/1ZX4BIwJTL0nVdkpRvLYDdk67jQfkRD_ErJWWHn-4KP8/edit) (Internal)

Our KPIs and processes follow industry best practices. We regularly iterate on new ideas and different strategies. The following articles can be helpful to explore new ways of Developer Evangelism:

- [Measuring Success and KPIs in Developer Relations - Community Contributed Outline](https://dev.to/tessamero/measuring-success-and-kpis-in-developer-relations-community-contributed-outline-1383)
- [Measuring the Impact of Your Developer Relations Team](https://openviewpartners.com/blog/measuring-the-impact-of-your-developer-relations-team/)
- [Developer evangelism and GitHub metrics - Or why stars are not the answer](https://devrel.net/strategy-and-metrics/developer-evangelism-github-metrics)
- [Developer Evangelism with Tessa Mero of Cisco](https://openchannel.io/blog/developer-evangelism-tessa-mero-cisco/)
- [What Do You Do as a Developer Advocate (🥑) at Elastic?](https://xeraa.net/blog/2020_what-do-you-do-as-a-developer-advocate-at-elastic/)
- [How to teach code](https://welearncode.com/teaching-code/) by Ali Spittel

## Useful links

1. [GitLab Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/)
1. [How to be an evangelist](/handbook/marketing/community-relations/developer-evangelism/how-to-be-an-evangelist/)
1. [How to submit a successful conference proposal](/handbook/marketing/community-relations/developer-evangelism/writing-cfps/)
1. [Consortiums we work with](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships)
1. [Speaking logistics](/handbook/marketing/community-relations/developer-evangelism/speaking-logistics/)
