---
layout: handbook-page-toc
title: "Territory Planning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 
The goal of reviewing your territory and tiering your accounts (customers AND prospects) is to enable you to prioritize your time and proactively work on the right customers with the right amount of effort. When your accounts are ranked effectively, you can work on a cadence that helps you grow IACV by spending the right amount of time depending on the potential and current value of the customer/prospect.

## Analyze Your Territory 
To get started, you'll need to understand how the territory looks. 

*Remember, keep it simple and iterate!* 

*Steps*
1. Start with the goal in mind (for example, build 2.5-3x quote pipeline)
1. Determine how your territory buys software / technology, how many developers they have
1. Understand the routes to market: Direct, Channel, and Programs 
1. Use [Demandbase](handbook/marketing/revenue-marketing/account-based-strategy/demandbase/), GitLab's targeting and personalization platform, to review intent data and ideal customer profiles  

Once you've organized your accounts and understand your routes to market, you can determine which resources to engage with to begin [Account Planning](/handbook/sales/account-planning/).

