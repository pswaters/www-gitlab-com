import Vue from 'vue/dist/vue.min.js'
import Slippers from 'slippers-ui/dist/slippersComponents.common.js'

// eslint-disable-next-line import/no-webpack-loader-syntax
const css = require("!raw-loader!sass-loader!../stylesheets/slippers.css.scss").default;

// Enable Vue devtools for development environments
if (process.env.NODE_ENV === 'development') {
    Vue.config.devtools = true;
}

// Register Slipper components with Vue
Object.keys(Slippers).forEach(name => {
    Vue.component(name, Slippers[name]);
})

document.addEventListener('DOMContentLoaded', function () {
    initializeSlippersVueInstances();
});

document.addEventListener('CookiebotOnLoad', function () {
    initializeSlippersVueInstances();
});

document.addEventListener('CookiebotOnDecline', function () {
    initializeSlippersVueInstances()
});

export function initializeSlippersWebComponent() {
    if (window.customElements) {
        customElements.define('slp-blog',
            class extends HTMLElement {
                constructor() {
                    super();
                    const template = document.getElementById('slp-blog').content;
                    const shadowRoot = this.attachShadow({ mode: 'open' });
                    shadowRoot.innerHTML = `<style>${css}</style>`;
                    shadowRoot.appendChild(template.cloneNode(true));
                }
            });
    }
}

// Find places we specifically want to mount a Vue instance, 
// labeled with `data-vue-target="true"`. Render a new Vue instance there. 
// In case multiple events fired (i.e. both `DOMContentLoaded` and `CookiebotOnLoad` fired),
// check whether or not the target already has a `__vue__` property, 
// which is the Vue instance accessor that Vue DevTools uses, but may change in Vue 3
// https://github.com/vuejs/vue/issues/5621
// Slightly different from the work in `source/javascripts/slippers.js` 
// because it queries the Shadow DOM inside `slp-blog`
export function initializeSlippersVueInstances() {
    const shadowHost = document.querySelector('slp-blog');
    const vueTargets = shadowHost.shadowRoot.querySelectorAll('[data-vue-target="true"]');
    for (let i = 0; i < vueTargets.length; i++) {
        const target = vueTargets[i]
        if (typeof target.__vue__ === "undefined") {
            new Vue({ el: vueTargets[i] });
        }
    }
}

initializeSlippersWebComponent();
